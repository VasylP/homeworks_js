"use strict";

/**
 Завдання
 Написати реалізацію кнопки "Показати пароль". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

 Технічні вимоги:
 У файлі index.html лежить розмітка двох полів вводу пароля.
 Після натискання на іконку поруч із конкретним полем -
 повинні відображатися символи, які ввів користувач,
 іконка змінює свій зовнішній вигляд.
 У коментарях під іконкою - інша іконка,
 саме вона повинна відображатися замість поточної.
 Коли пароля не видно - іконка поля має виглядати як та,
 що в першому полі (Ввести пароль)
 Коли натиснута іконка, вона має виглядати, як та,
 що у другому полі (Ввести пароль)
 Натиснувши кнопку Підтвердити,
 потрібно порівняти введені значення в полях
 Якщо значення збігаються – вивести модальне вікно
 (можна alert) з текстом – You are welcome;
 Якщо значення не збігаються - вивести під
 другим полем текст червоного кольору Потрібно ввести однакові значення
 Після натискання на кнопку сторінка не повинна перезавантажуватись
 Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.
 */

// const passwordForm = document.querySelector('.password-form');
// const inputPassword = passwordForm.querySelector('#password');
// const repeatPassword = passwordForm.querySelector('#password-repeat');
// const btn = passwordForm.querySelector(".btn");
// const iconSeePass = passwordForm.querySelectorAll('.icon-password');
// const input = document.querySelectorAll('input');
//
// const p = document.createElement('p');
// p.style.width = "200px";
// p.style.color = "red";
// p.style.display = 'none';
// p.innerText = "Потрібно ввести однакові значення";
// repeatPassword.append(p);

/** Variant 2 */
// iconSeePass.forEach((icon) => {
//     let inputId;
//     icon.addEventListener('click', () => {
//         inputId = document.querySelector(icon.getAttribute('data-id'));
//             if (inputId.getAttribute('type') === 'password'){
//                 inputId.setAttribute('type', 'text');
//                 icon.classList.remove('fa-eye');
//                 icon.classList.add('fa-eye-slash');
//             } else {
//                 inputId.setAttribute('type', 'password');
//                 icon.classList.remove('fa-eye-slash');
//                 icon.classList.add('fa-eye');
//             }
//         });
// });
//
// btn.addEventListener('click', () => {
//     if (document.querySelector('#pass').value === document.querySelector('#repeat').value){
//         alert('You are welcome');
//     } else {
//         p.style.display = '';
//     }
// });

/** Variant 3 */
// passwordForm.addEventListener('click', (e) => {
//    if (e.target.closest('.icon-password')) {
//        let inputId;
//        inputId = document.querySelector(e.target.getAttribute('data-id'));
//        if (inputId.getAttribute('type') === 'password'){
//            inputId.setAttribute('type', 'text');
//            e.target.classList.remove('fa-eye');
//            e.target.classList.add('fa-eye-slash');
//        } else {
//            inputId.setAttribute('type', 'password');
//            e.target.classList.remove('fa-eye-slash');
//            e.target.classList.add('fa-eye');
//        }
//    }
//     if (e.target.closest('.btn')){
//         if (!document.querySelector('#pass').value || !document.querySelector('#repeat').value){
//             alert('Please enter value');
//         } else if (document.querySelector('#pass').value === document.querySelector('#repeat').value){
//             alert('You are welcome');
//         } else {
//             p.style.display = '';
//         }
//     }
// });

// passwordForm.addEventListener('click', (e) => {
//     if (e.target.closest('.icon-password')) {
//         let inputId;
//         inputId = document.querySelector(e.target.getAttribute('data-id'));
//         if (inputId.getAttribute('type') === 'password'){
//             inputId.setAttribute('type', 'text');
//             e.target.classList.remove('fa-eye');
//             e.target.classList.add('fa-eye-slash');
//         } else {
//             inputId.setAttribute('type', 'password');
//             e.target.classList.remove('fa-eye-slash');
//             e.target.classList.add('fa-eye');
//         }
//     }
//     // if (e.target.closest('.btn')){
//     //     if (!document.querySelector('#pass').value || !document.querySelector('#repeat').value){
//     //         alert('Please enter value');
//     //     } else if (document.querySelector('#pass').value === document.querySelector('#repeat').value){
//     //         alert('You are welcome');
//     //     } else {
//     //         p.style.display = '';
//     //     }
//     //  }
// });
//
// passwordForm.addEventListener('submit', (event) =>{
//         if (!document.querySelector('#pass').value || !document.querySelector('#repeat').value){
//             alert('Please enter value');
//             event.preventDefault();
//         } else if (document.querySelector('#pass').value === document.querySelector('#repeat').value){
//             alert('You are welcome');
//         } else {
//             p.style.display = '';
//             event.preventDefault();
//         }
// });

/** Variant - 1 */
/*
inputPassword.addEventListener('click', (event) => {
    if (event.target.closest('.fa-eye')) {
        event.target.classList.remove('fa-eye');
        event.target.classList.add('fa-eye-slash');
        inputPassword.querySelector('input').type = "text";
    } else {
        event.target.classList.remove('fa-eye-slash');
        event.target.classList.add('fa-eye');
        inputPassword.querySelector('input').type = "password";
    }
});

repeatPassword.addEventListener('click', (event) => {
        if (event.target.closest('.fa-eye')) {
            event.target.classList.remove('fa-eye');
            event.target.classList.add('fa-eye-slash');
            repeatPassword.querySelector('input').type = "text";
        } else {
            event.target.classList.remove('fa-eye-slash');
            event.target.classList.add('fa-eye');
            repeatPassword.querySelector('input').type = "password";
        }
});

btn.addEventListener('click', () => {
    if (repeatPassword.querySelector('input').value === inputPassword.querySelector('input').value){
        alert('You are welcome');
    } else {
        p.style.display = '';
    }
});*/

const passwordForm = document.querySelector('.password-form');

passwordForm.addEventListener('click', (e) => {
   if (e.target.closest('.icon-password')) {
       let inputId;
       inputId = document.querySelector(e.target.getAttribute('data-id'));
       if (inputId.getAttribute('type') === 'password'){
           inputId.setAttribute('type', 'text');
           e.target.classList.remove('fa-eye');
           e.target.classList.add('fa-eye-slash');
       } else {
           inputId.setAttribute('type', 'password');
           e.target.classList.remove('fa-eye-slash');
           e.target.classList.add('fa-eye');
       }
   }
});

passwordForm.addEventListener('submit', (event) =>{
    if (!document.querySelector('#pass').value || !document.querySelector('#repeat').value){
        alert('Please enter value');
        event.preventDefault();
    } else if (document.querySelector('#pass').value === document.querySelector('#repeat').value){
        alert('You are welcome');
    } else {
    document.querySelector('#repeat').parentElement.insertAdjacentHTML(
        "beforeend",
        `<div class="error-message" style="color: red">
                Потрібно ввести однакові значення
            </div>`
    );
    event.preventDefault();
    }
});

document.querySelectorAll('input').forEach(element => {
    element.addEventListener('focus', () => {
        if (document.querySelector('.error-message')){
            document.querySelector('.error-message').remove();
        }
    });
});