"use strict";
/*

   * Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
   * Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
   * Що таке явне та неявне приведення (перетворення) типів даних у JS?

    * Цикли необхідні в програмуванні, для того, щоб не виконувати одну і ту саму роботу багато разів
    (тобто не прописувати один і той самий код велику кількість разів).
    Якщо необхідно вивести якусь певну кількість разів змінну або виконувати якийсь певний код до поки
    виконується якась конкретна умова, то це простіше зробити через використання циклу.

    *
    - цикл for - краще використовувати, якщо безпосередньо в циклу одразу оголошується перемінна;

    -цикл while - краще використовувати коли перемінна була задана і для неї потрібно використати цикл;

    -цикл do while - використовуєтьсяґ у випадку, якщо необхідно щоб цикл виконався хочаб раз, а потім уже
    перевіряв умову для подальшого його виконання чи припинення;

    * Неявне перетворення типів - це автоматичне перетворення двох значень, що
    використовуються у виразі і мають різний тип, до одного спілного типу.
       Явне перетворення типів - це коли ми за допомогою певних команд - умисне переводимо дані з одного типу
    в інший. Це може бути, для прикладу, умисне перетворення двох різних типів данних - наприклад в Boolean.

*/



let userNumber = +prompt("Please, enter your number");

while (!userNumber || userNumber < 0 || !Number.isInteger(userNumber)) {
    userNumber = +prompt("Please, enter your number, correctly");
}

if (userNumber >= 1 && userNumber <= 4) {
    console.log("Sorry, no numbers");
} else {
    for (let i = 0; i <= userNumber; i++) {
        if (!(i % 5)) {
            console.log(i);
        }
    }
}




/*
Отримати два числа, m і n. Вивести в консоль усі прості числа
(http://uk.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE)
в діапазоні від m до n (менше із введених чисел буде m, більше буде n).
Якщо хоча б одне з чисел не відповідає умовам валідації, зазначеним вище, вивести повідомлення про помилку
і запитати обидва числа знову.
*/


/**let m = +prompt("Enter start number, start number must be more 1");

let n = +prompt("Enter last number, last number must be more to start number");

while (!+m || !+n || m < 2 || n < m || !Number.isInteger(m) || !Number.isInteger(n)) {
    m = +prompt("Enter start number");
    n = +prompt("Enter last number");
}

for (let i = m; i <= n; i++) {
    let isPrime = true;
    for (let j = 2; j <= i/2; j++){
        if (i % j) continue;
            isPrime = false;
            break;
    }
    if (isPrime) console.log(i);
}*/

// for (let i = m; i <= n; i++) {
//     let edge = Math.sqrt(i);
//     let isPrime = true;
//     for (let j = 2; j <= edge; j++) {
//         if (i % j) continue;
//         isPrime = false;
//         break;
//     }
//     if (isPrime) console.log(i);
// }
