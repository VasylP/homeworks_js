"use strict";

/** Теоретичні питання
 1) Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
 2) Які засоби оголошення функцій ви знаєте?
 3) Що таке hoisting, як він працює для змінних та функцій?

1) Екранування - це використання спеціальних символів для коректного відображення вмісту (тексту).
 2) Оголошення функції складається з : ім'я функції, спику параметрів та інструкцій які будуть
 виконані після виклику функції.
 3) hoisting - це термін, який говорить про те, що оголошення змінних чи функцій фізично переміщується
 на початок коду. Насправді вони залишаються в коді на своєму місці (там де були оголошені),
 але потрапляють в пам'ять в процесі фази компіляції і це дозволяє використовувати, наприклад,
 функцію до її оголошення.

 Завдання
 Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем.
 Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

 Технічні вимоги:
 Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser())
 і доповніть її наступним функціоналом:
 При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy)
 і зберегти її в полі birthday.
 Створити метод getAge() який повертатиме скільки користувачеві років.
 Створити метод getPassword(), який повертатиме першу літеру імені користувача
 у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження.
 (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
 Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та
 getPassword() створеного об'єкта.
 */

function createNewUser (){
    const newUser = {
        userName: {writable: false, configurable: true, value: ""},
        userLastName: {writable: false, configurable: true, value: ""},
        birthday: {writable: false, configurable: true, value: ""},
        setName (newName) {
            Object.defineProperty(this, 'userName', {value: newName});
        },
        setSurname (newLastName) {
            Object.defineProperty(this, 'userLastName', {value: newLastName});
        },
        setBirth (newBirthDate) {
            Object.defineProperty(this, 'birthday', {value: newBirthDate});
        },
        getLogin () {
            return this.userName.toLowerCase().charAt(0) + this.userLastName.toLowerCase();
        },
        getAge() {
            let date = (`${this.birthday.substring(6, 10)}-${this.birthday.substring(3, 5)}-${this.birthday.substring(0, 2)}`);
            let birthdayDate = new Date(date);
            let nowDate = new Date();
            let now = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate());
            let thisYearBirth = new Date(nowDate.getFullYear(), birthdayDate.getMonth(), birthdayDate.getDate());
            let result;
            result = nowDate.getFullYear() - birthdayDate.getFullYear();
            if (now < thisYearBirth){
                return result = result - 1;
            }
            return result;
        },
        getPassword() {
            return this.userName.toUpperCase().charAt(0) + this.userLastName.toLowerCase() + this.birthday.substring(6, 10);
        },
    }
    newUser.setName(prompt("Будь ласка введіть своє ім'я"));
    newUser.setSurname(prompt("Будь ласка введіть своє прізвище"));
    newUser.setBirth(prompt("Будь ласка введіть свою дату народження", "dd.mm.yyyy"));
    console.log(newUser.getLogin());
    console.log(newUser.getAge());
    console.log(newUser.getPassword());
    return newUser;
}
// createNewUser();
console.log(createNewUser());