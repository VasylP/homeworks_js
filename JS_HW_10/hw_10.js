"use strict";

/**
 Завдання
 Реалізувати перемикання вкладок (таби) на чистому Javascript.

 Технічні вимоги:
 У папці tabs лежить розмітка для вкладок.
 Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки.
 При цьому решта тексту повинна бути прихована. У коментарях зазначено,
 який текст має відображатися для якої вкладки.

 Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
 Потрібно передбачити, що текст на вкладках може змінюватись,
 і що вкладки можуть додаватися та видалятися.
 При цьому потрібно, щоб функція, написана в джаваскрипті,
 через такі правки не переставала працювати.
 */

/** Need for work with all variant*/
const contentDiv = document.querySelector('.centered-content');
const tabs = document.querySelector('.tabs');
let tabsTitle = document.querySelectorAll(".tabs-title");
const tabsContent = document.querySelector('.tabs-content');
let tabsContentItem = tabsContent.querySelectorAll('li');

/** Need for work with third and second variant*/
tabsContent.style.display = "none";
tabsTitle.forEach(item => {
    if (item.classList.contains('active')){
        item.classList.remove('active');
    }
})

/** Third Variant with test */
const nullDiv = document.createElement('div');
nullDiv.textContent = "Приносимо вибачення! Контент з'явиться найближчим часом.";
tabs.after(nullDiv);

contentDiv.addEventListener('click', (event) => {
    nullDiv.style.display = 'none';
    if (event.target.closest(".tabs-title")) {
        let currentTitle = event.target;
        tabsContent.style.display = "";
        let tabTitleData = currentTitle.getAttribute("data-tab");
        let currentContent = document.querySelector(tabTitleData);
        if (!currentTitle.classList.contains('active')) {
            document.querySelectorAll(".tabs-title").forEach(item => {
                item.classList.remove('active');
            });
            tabsContent.querySelectorAll('li').forEach(item => {
                item.style.display = "none";
            });
        }
        currentTitle.classList.add('active');
        if (currentContent !== null){
        currentContent.style.display = "";
        } else {
            nullDiv.style.display = '';
        }
    }
});

document.querySelector(".tabs-title").click();
/** Test create elements on JS */
/*const newTab = document.createElement('li');
newTab.setAttribute('data-tab', "#tab_9");
newTab.className = "tabs-title";
newTab.textContent = "TEST_JS_CREATE";
tabs.append(newTab);

const newContent = document.createElement("li");
newContent.id = 'tab_9';
newContent.textContent = "TEST CREATE ON JAVASCRIPT :)";
newContent.style.display = 'none';
tabsContent.append(newContent);*/

/** Second Variant */
/*contentDiv.addEventListener('click', (event) => {
    if (event.target.closest(".tabs-title")) {
        let currentTitle = event.target;
        tabsContent.style.display = "";
        let tabTitleData = currentTitle.getAttribute("data-tab");
        let currentContent = document.querySelector(tabTitleData);
        if (!currentTitle.classList.contains('active')) {
            tabsTitle.forEach(item => {
                item.classList.remove('active');
            });
            tabsContentItem.forEach(item => {
                item.style.display = "none";
            });
        }
        currentTitle.classList.add('active');
        currentContent.style.display = "";
    }
});

document.querySelector(".tabs-title").click();*/

/** First Variant */
/*tabsTitle.forEach(item => {
    let currentBtn;
    tabsContent.style.display = "none";
    item.classList.remove('active');
    item.addEventListener('click', () => {
        currentBtn = item;
        tabsContent.style.display = "";
        let tabData = currentBtn.getAttribute("data-tab");
        let currentTab = document.querySelector(tabData);
        if (!currentBtn.classList.contains('active')){
        tabsTitle.forEach(item => {
            item.classList.remove('active');
        });
        tabsContentItem.forEach(item => {
            item.style.display = "none";
            item.classList.remove('active');
        });
        currentBtn.classList.add('active');
        currentTab.classList.add('active');
        currentTab.style.display = "";
        }
    });
});

document.querySelector(".tabs-title").click();*/

