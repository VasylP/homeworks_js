"use strict";

/** Теоретичні питання:

 1. Опишіть своїми словами як працює метод forEach.
 2. Як очистити масив?
 3. Як можна перевірити, що та чи інша змінна є масивом?

 1. forEach працює по принципу як цикл for. Цей метод дозволяє перебирати дані, що містяться в масиві.
 Але цей метод буде перебирати всі дані, які містяться в масиві.
 2. За допомогою array.splice(0) - можна очистити масив.
 3. Array.isArray() - дозволяє перевірити чи є змінна масивом.

 */
/**
 Завдання:

 Реалізувати функцію фільтру масиву за вказаним типом даних.
 Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

 Технічні вимоги:
 Написати функцію filterBy(), яка прийматиме 2 аргументи.
 Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
 Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент,
 за винятком тих, тип яких був переданий другим аргументом.
 Тобто якщо передати масив ['hello', 'world', 23, '23', null],
 і другим аргументом передати 'string', то функція поверне масив [23, null].
 */

/*
let array = ['hello', 'world', 23, '23', null];

function filterBy(arr, dataType) {
    let newArr;
    newArr = arr.filter(function (element){
        return typeof element !== dataType;
    });
    return newArr;
}

console.log(filterBy(array, 'string'));
*/


let array = ['hello', 'world', 23, '23', null];

function filterBy(arr, dataType) {
    let newArr;
    newArr = arr.filter((element) => typeof element !== dataType);
    return newArr;
}

console.log(filterBy(array, 'string'));
