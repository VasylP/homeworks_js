"use strict";

/**
 -- Завдання --
 Намалювати на сторінці коло за допомогою параметрів, які введе користувач.
 Завдання має бути виконане на чистому Javascript без використання бібліотек
 типу jQuery або React.

 Технічні вимоги:
 - При завантаженні сторінки – показати на ній кнопку з текстом "Намалювати коло".
 Дана кнопка повинна бути єдиним контентом у тілі HTML документа,
 решта контенту потрібно створити і додати на сторінку за допомогою Javascript.
 - При натисканні на кнопку "Намалювати коло" показувати
 одне поле введення - діаметр кола.
 - При натисканні на кнопку "Намалювати" створити на сторінці
 100 кіл (10*10) випадкового кольору. При кліку на конкретне
 коло - це коло має зникати, у своїй порожнє місце заповнюватися,
 тобто інші кола зрушуються вліво.
 - У вас може виникнути бажання поставити обробник події
 на кожне коло для його зникнення. Це неефективно, так не треба робити.
 На всю сторінку має бути лише один обробник подій, який це робитиме.
 */

const wrapper = document.querySelector('.wrapper');
const btn = document.querySelector('.btn');

const form = document.createElement('form');
form.classList.add('questions-field');
form.innerText = "Введіть діаметр кола";
form.style.cssText = `position: absolute; top: 50%; left: 50%; transform:translate(-50%, -50%);
        border: 2px solid #000000; background-color: #0002046b; width: 20vw; text-align: center; color: lightblue;
        padding: 30px; border-radius: 10px; display: none`;
const input = document.createElement('input');
input.setAttribute('type', 'text');
input.style.margin = '20px';
input.setAttribute('type', 'number');
form.append(input);
const btn2 = document.createElement('button');
btn2.classList.add('btn2');
btn2.innerText = 'Намалювати';
btn2.style.cssText = `border: 2px solid #000000; border-radius: 10px; width: 150px; cursor: pointer;
background-color: chartreuse; color: chocolate;`
form.append(btn2);
wrapper.prepend(form);

function getColor (){
    return "#" + (Math.random().toString(16) + "000000").substring(2, 8).toUpperCase();
}

function getFigure (circleSize, circleColor, node = wrapper) {
    const newDiv = document.createElement('div');
    newDiv.classList.add('circle');
    node.append(newDiv);
    newDiv.style.cssText = `width: ${circleSize}px; height: ${circleSize}px; background-color: ${circleColor};
    border-radius: 50px; margin: 1px`;
}

wrapper.addEventListener('click', (event) => {
    if (event.target.classList.contains('btn')){
        form.style.display = '';
        btn.style.display = 'none';
    }
    if (event.target.classList.contains('btn2')){
        for (let i = 0; i < 100; i++) {
            getFigure(input.value, getColor());
        }
        form.remove();
    }
    if (event.target.classList.contains('circle')){
        event.target.classList.add('active');
        wrapper.querySelectorAll('div.active').forEach(element => {
            element.remove();
        });
    }
});

document.body.addEventListener('dblclick', (event) => {
    const btnClear = document.createElement('button');
    btnClear.classList.add('clear');
    btnClear.style.cssText = `border: 2px solid #000000; border-radius: 10px; width: 150px; 
    height: 30px; cursor: pointer; background-color: chartreuse; color: chocolate; position: absolute; 
    top: 5%; left: 2%;`;
    btnClear.innerText = "Очистити поле";
    document.body.prepend(btnClear);
    const btnAdd = document.createElement('button');
    btnAdd.classList.add('add');
    btnAdd.style.cssText = `border: 2px solid #000000; border-radius: 10px; width: 150px; 
    cursor: pointer; background-color: yellow; color: darkblue; 
    position: absolute; top: 15%; left: 2%;`;
    btnAdd.innerText = "Додати ще 100 кульок :)";
    document.body.prepend(btnAdd);
    if (event.target.classList.contains('clear')){
        wrapper.querySelectorAll('div.circle').forEach(element => {
            element.remove();
        });
    }
    if (event.target.classList.contains('add')){
        wrapper.append(form);
    }
});