"use strict";

/** Реализовать функцию полного клонирования объекта.

 Задача должна быть реализована на языке javascript,
 без использования фреймворков и сторонник библиотек (типа Jquery).

 Технические требования:
 Написать функцию для рекурсивного полного клонирования объекта (без единой передачи по ссылке,
 внутренняя вложенность свойств объекта может быть достаточно большой).
 Функция должна успешно копировать свойства в виде объектов и массивов на любом уровне вложенности.
 В коде нельзя использовать встроенные механизмы клонирования, такие как функция Object.assign() или спред-оператор.
 */

// const car = {
//     brand: "Reno",
//     model: "Megane",
//     year: 2007,
//     characteristics: {
//         engine: "2 liters",
//         fuel: "gasoline",
//         transmission: "mechanics",
//         color: {
//           carColor: "blue",
//           interior: "gray",
//         },
//     },
//     type: "wagon",
// };

function getCloneObj (newObj, obj) {
    for (let key in obj) {
        if ((typeof obj[key]) == "object") {
            newObj[key] = getCloneObj({}, obj[key]);
        } else {
            newObj[key] = obj[key];
        }
    }
    return newObj;
}

// let newCar = getCloneObj({}, car);
// console.log(newCar);