"use strict";
/**
Завдання
Реалізувати функцію, яка дозволить оцінити,
 чи команда розробників встигне здати проект до настання дедлайну.
 Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

    Технічні вимоги:
    Функція на вхід приймає три параметри:
    масив із чисел, що становить швидкість роботи різних членів команди.
 Кількість елементів у масиві означає кількість людей у команді.
 Кожен елемент означає скільки стор поінтів (умовна оцінка складності виконання завдання)
 може виконати даний розробник за 1 день.
    масив з чисел, який є беклогом (список всіх завдань, які необхідно виконати).
 Кількість елементів у масиві означає кількість завдань у беклозі.
 Кожне число в масиві означає кількість сторі поінтів, необхідні виконання даного завдання.
    Дата дедлайну (об'єкт типу Date).
Після виконання, функція повинна порахувати,
 чи команда розробників встигне виконати всі завдання з беклогу до настання дедлайну
 (робота ведеться починаючи з сьогоднішнього дня).
 Якщо так, вивести на екран повідомлення: Усі завдання будуть успішно виконані за ?
 днів до настання дедлайну!. Підставити потрібну кількість днів у текст.
 Якщо ні, вивести повідомлення Команді розробників доведеться витратити додатково ?
 годин після дедлайну, щоб виконати всі завдання в беклозі
Робота триває по 8 годин на день по будніх днях
 */
const teamWorkSpeed = [2, 4, 3, 1, 6,];

const backlogItem = [2, 9, 5, 6, 4, 3, 8, 7, 10, 1, 9,];

const deadline = new Date(2022, 7, 18);
// const deadline = new Date(2022, 7, 25);

function teamPoints (teamList){
    let teamPoint;
    teamPoint = teamList.reduce(function (prevValue, currentValue) {
        return prevValue + currentValue;
    });
    return teamPoint;
}

function  backLogPoints ( backLogList){
    let backLogPoints;
    backLogPoints = backLogList.reduce(function (prevValue, currentValue) {
        return prevValue + currentValue;
    });
    return backLogPoints;
}

function gateVyh (start, end) {
    let timeDaysFromStartToEndWorkDate = (end.getTime() - start.getTime()) /
        (24 * 60 * 60 * 1000);
    let vyh = 0;
    for (let i = 0; i < timeDaysFromStartToEndWorkDate; i++) {
        let getDay = new Date();
        getDay.setTime(start.getTime() + ((1000 * 3600 * 24) * i));
        let result;
        result = getDay.getDay()
        if (result === 0 || result === 6) {
            vyh++;
        }
    }
    return vyh;
}

function deadlineTime (team, backLog, deadLineData) {
    let pointInHourTeam = teamPoints(team) / 8;
    let pointBackLogTime = backLogPoints(backLog) / pointInHourTeam * 3600000;
    let startWorkDate = new Date();
    let timeToDeadline = (deadLineData.getTime() - startWorkDate.getTime())  - (gateVyh(startWorkDate,deadLineData) * (1000*3600*24));
    if ( (timeToDeadline / (1000*3600*24)) >= (pointBackLogTime / (1000*3600*8))) {
        let result = (timeToDeadline / (1000*3600*24)) - (pointBackLogTime / (1000*3600*8)) ;
        return alert("Усі завдання будуть успішно виконані за " + Math.ceil(result) + " днів до настання дедлайну!")
    } else {
        let moreTime = (pointBackLogTime - timeToDeadline) / 3600000;
        return alert("Команді розробників доведеться витратити додатково " +
            Math.ceil(moreTime) + " годин після дедлайну," +
            " щоб виконати всі завдання в беклозі");
    }
}
deadlineTime(teamWorkSpeed, backlogItem, deadline);


/*function teamPoints (teamList){
    let teamPoint;
    teamPoint = teamList.reduce(function (prevValue, currentValue) {
        return prevValue + currentValue;
    });
    return teamPoint;
}

function  backLogPoints ( backLogList){
    let backLogPoints;
    backLogPoints = backLogList.reduce(function (prevValue, currentValue) {
        return prevValue + currentValue;
    });
    return backLogPoints;
}

function deadlineTime (team, backLog, deadLineData) {
    let pointInHourTeam = teamPoints(team) / 8;
    let pointBackLogTime = backLogPoints(backLog) / pointInHourTeam * 3600000;
    let startWorkDate = new Date();
    let timeDaysFromStartToEndWorkDate = (deadLineData.getTime() - startWorkDate.getTime()) /
        (24*60*60*1000);
    let vyh = 0;
    for (let i = 0; i < timeDaysFromStartToEndWorkDate; i++){
        let getDay = new Date();
        getDay.setTime(startWorkDate.getTime()+((1000*3600*24)*i));
        let result;
        result = getDay.getDay()
        if (result === 0 || result === 6){
            vyh ++;
        }
    }
    let timeToDeadline = (deadLineData.getTime() - startWorkDate.getTime())  - (vyh * (1000*3600*24));
    if ( (timeToDeadline / (1000*3600*24)) >= (pointBackLogTime / (1000*3600*8))) {
        let result = (timeToDeadline / (1000*3600*24)) - (pointBackLogTime / (1000*3600*8)) ;
        return alert("Усі завдання будуть успішно виконані за " + Math.ceil(result) + " днів до настання дедлайну!")
    } else {
        let moreTime = (pointBackLogTime - timeToDeadline) / 3600000;
        return alert("Команді розробників доведеться витратити додатково " +
            Math.ceil(moreTime) + " годин після дедлайну," +
            " щоб виконати всі завдання в беклозі");
    }
}
deadlineTime(teamWorkSpeed, backlogItem, deadline);*/

/*
function deadlineTime (team, backLog, deadLineData) {
    let teamPoints;
    let backLogPoints;
    teamPoints = team.reduce(function (prevValue, currentValue) {
        return prevValue + currentValue;
    });
    backLogPoints = backLog.reduce(function (prevValue, currentValue) {
        return prevValue + currentValue;
    });
    let pointInHourTeam = teamPoints / 8;
    let pointBackLogTime = backLogPoints / pointInHourTeam * 3600000;
    let oneDayWork = 8 * 3600000;
    let daysForBackLog = pointBackLogTime / oneDayWork;
    daysForBackLog = Math.ceil(daysForBackLog);
    let nowDate = Date.now();
    let teamEndWork = nowDate + daysForBackLog;
    deadLineData = Date.parse(deadLineData);
    if (teamEndWork < deadLineData){
        return alert("Усі завдання будуть успішно виконані за " +
            Math.ceil((deadLineData - teamEndWork) / 86400000 / oneDayWork) + " днів до настання дедлайну!")
    } else {
        return alert("Команді розробників доведеться витратити додатково " +
            Math.ceil((teamEndWork - deadLineData) / oneDayWork) +" годин після дедлайну," +
            " щоб виконати всі завдання в беклозі")
    }
}

deadlineTime(teamWorkSpeed, backlogItem, deadline);*/