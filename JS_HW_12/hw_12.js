"use strict";

/**
 -- Теоретичні питання --
 1. Чому для роботи з input не рекомендується використовувати клавіатуру?

    Клавіатуру не рекомендують використовувати, оскілки на сьогодняшній день
    є альтернативні засоби вводу даних в поле input, наприклад такі, як голосове введення.

 -- Завдання --
 Реалізувати функцію підсвічування клавіш.
 Завдання має бути виконане на чистому Javascript
 без використання бібліотек типу jQuery або React.

 - Технічні вимоги: -
 У файлі index.html лежить розмітка для кнопок.
 Кожна кнопка містить назву клавіші на клавіатурі
 Після натискання вказаних клавіш - та кнопка,
 на якій написана ця літера, повинна фарбуватися в синій колір.
 При цьому якщо якась інша літера вже раніше була пофарбована
 в синій колір - вона стає чорною.
 Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір.
 Далі, користувач натискає S, і кнопка S забарвлюється в синій колір,
 а кнопка Enter знову стає чорною.
 */
/** V -1 */
// document.body.addEventListener('keyup', (event) => {
//     document.querySelectorAll('.btn').forEach(e => {
//        if (event.key === e.innerText.toLowerCase() ||
//            event.key === e.innerText.toUpperCase() ||
//            event.key === e.innerText){
//            e.style.backgroundColor = 'blue';
//        } else {
//            e.style.backgroundColor = '#000000';
//        }
//     });
// });
/** V -2 */
document.body.addEventListener('keyup', (event) => {
    document.querySelectorAll('.btn').forEach(e => {
        if (event.key === e.innerText.toLowerCase() ||
            event.key === e.innerText.toUpperCase() ||
            event.key === e.innerText){
            e.classList.add('active');
            document.querySelector('.active').style.backgroundColor = 'blue';
        } else {
            e.classList.remove('active');
            e.style.backgroundColor = '';
        }
    });
});