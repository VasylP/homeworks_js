"use strict";
/*
    *Які існують типи даних у Javascript?
    *У чому різниця між == і ===?
    *Що таке оператор?

    * У JS існують такі типи даних:
    - Number
    - Bigint
    - String
    - Boolean
    - undefined
    - null
    - Object
    _ Symbol

    * === перевіряє на рівність дві велечини не змінюючи їх тип
        == перед порівнянням приводить величини до загального типу

    *  Оператор - це елемент, який задає опис дій , які необхідно виконати.
    */

let userName = prompt("Please, enter your name");

if (userName === null) {
    userName = " ";
}

let userAge = prompt("Please, enter your age");

if (userAge === null) {
    userAge = 0;
}

while (userAge < 0 || isNaN(userAge) || !userName || userName === " ") {
    userName = prompt('Please, enter your name, correctly', userName);
    userAge = +prompt('Please, enter your age, correctly', userAge);
}

// switch (true) {
//     case (userAge < 18):
//         alert("You are not allowed to visit this website");
//         break;
//     case (userAge > 22):
//         alert("Welcome, " + userName);
//         break;
//     case (userAge >= 18 || userAge <= 22):
//         if (confirm("Are you sure you want to continue?")) {
//             alert("Welcome, " + userName);
//         } else {
//             alert("You are not allowed to visit this website");
//         }
//         break;
// }


if (userAge > 22){
    alert("Welcome, " + userName);
} else if (userAge < 18){
    alert("You are not allowed to visit this website");
} else {
    alert (confirm("Are you sure you want to continue?") ? "Welcome, " + userName : "You are not allowed to visit this website");
}