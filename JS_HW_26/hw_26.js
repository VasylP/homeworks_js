"use strict"

/**
 Завдання
 Реалізувати слайдер на чистому Javascript.

 Технічні вимоги:

 Створити HTML розмітку, що містить кнопки Previous, Next, та картинки (6 штук),
 які будуть перегортатися горизонтально.
 На сторінці має бути видно лише одну картинку.
 Зліва від неї буде кнопка Previous, праворуч – Next.
 Після натискання на кнопку Next - має з'явитися нова картинка,
 що випливає зі списку.
 Після натискання на кнопку Previous - має з'явитися попередня картинка.
 Слайдер може бути нескінченним, тобто. якщо на початку натиснути на кнопку Previous,
 то з'явиться остання картинка, а якщо натиснути на Next,
 коли видима - остання картинка, то буде видно першу картинку.
 Приклад роботи слайдера можна побачити тут (перший приклад).
 */

const images = document.querySelectorAll('.slider-img');

let counter = 0;
let step = 600;

function showPrevImg () {
    counter--;
    if (counter < 0) {
        counter = images.length-1;
    }
    images.forEach(element => {
        element.style.transform = "translateX(" + (-step * counter) + "px";
        element.classList.add('animation');
    });
}

function showNextImg () {
    counter++;
    if (counter >= images.length) {
        counter = 0;
    }
    images.forEach(element => {
        element.style.transform = "translateX(" + (-step * counter) + "px";
        element.classList.add('animation');
    });
}

function removeClass () {
    images.forEach(element => {
        element.classList.remove('animation');
    });
}

document.body.addEventListener('click', (e) => {
    if (e.target.closest('.btn-prev')){
        showPrevImg();
        setTimeout(removeClass, 1000);
    }
    if (e.target.closest('.btn-next')){
        showNextImg();
        setTimeout(removeClass, 1000);
    }
});