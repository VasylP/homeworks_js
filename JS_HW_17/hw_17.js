"use strict";

/**Завдання
 Створити об'єкт "студент" та проаналізувати його табель.
 Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

 Технічні вимоги:
 Створити порожній об'єкт student, з полями name та lastName.
 Запитати у користувача ім'я та прізвище студента, отримані значення записати у відповідні поля об'єкта.
 У циклі запитувати у користувача назву предмета та оцінку щодо нього.
 Якщо користувач натисне Cancel при n-питанні про назву предмета, закінчити цикл.
 Записати оцінки з усіх предметів як студента tabel.
 порахувати кількість поганих (менше 4) оцінок з предметів.
 Якщо таких немає, вивести повідомлення "Студент переведено на наступний курс".
 Порахувати середній бал з предметів. Якщо він більше 7 – вивести повідомлення Студенту призначено стипендію.
 */

const student = {
    name: " ",
    lastName: " ",
    table: {},
    getSum () {
        let sum = 0;
        for (let key in this.table) {
            sum += this.table[key];
        }
        return sum;
    },
    getAverageMark() {
        let result;
        for (let key in this.table) {
            result = this.getSum() / Object.keys(this.table).length;
        }
        return result;
    },
    getLength () {
        let length = 0;
        for (let key in this.table) {
            if (this.table[key] < 4) length ++;
        }
        return length;
    },
    getResult () {
        for (let key in this.table) {
            switch (true) {
                case this.table[key] < 4 :
                    return alert("Кількість поганих (менше 4) оцінок: " + this.getLength());
                default :
                    if (this.getAverageMark() <= 7) {
                        return alert(this.name + " " + this.lastName + " переведено на наступний курс");
                    } else {
                        alert(this.name + " " + this.lastName + " переведено на наступний курс");
                        return alert(this.name + " " + this.lastName + " призначено стипендію");
                    }
            }
        }
    },
}
let studentName = prompt("Введіть ім'я студента");
let studentLastName = prompt("Введіть прізвище студента");

student.name = studentName;
student.lastName = studentLastName;

function getProperty (subjectName, subjectMark) {
    return student.table[subjectName] = subjectMark;
}

let subject;
let mark;

do {
    subject = prompt("Введіть назву предмету");
    if (subject != null){
        mark = +prompt("Введіть оцінку");
        getProperty(subject, mark);}
} while (subject != null);

student.getResult();
/*
const student = {
    name: " ",
    lastName: " ",
}

let studentName = prompt("Введіть ім'я студента");
let studentLastName = prompt("Введіть прізвище студента");

student.name = studentName;
student.lastName = studentLastName;

student.table = {};

function getProperty (subjectName, subjectMark) {
    return student.table[subjectName] = subjectMark;
}

let subject;
let mark;

do {
    subject = prompt("Введіть назву предмету");
    if (subject != null){
    mark = +prompt("Введіть оцінку");
    getProperty(subject, mark);}
} while (subject != null);

function getSum (obj) {
    let sum = 0;
    for (let key in obj) {
        sum += obj[key];
    }
    return sum;
}

function getAverageMark(obj) {
    let result;
    for (let key in obj) {
        result = getSum(student.table) / Object.keys(obj).length;
    }
    return result;
}

function getLenght (obj) {
    let lenght = 0;
    for (let key in obj) {
        if (obj[key] < 4) lenght ++;
    }
    return lenght;
}

function getResult (obj) {
    for (let key in obj) {
        // if (obj[key] < 4){
        //     return alert("Кількість поганих (менше 4) оцінок: " + getLenght(student.table));
        // } else if (getAverageMark(student.table) <= 7) {
        //     return alert(student.name + " " + student.lastName + " переведено на наступний курс");
        // } else {
        //     alert(student.name + " " + student.lastName + " переведено на наступний курс");
        //     alert(student.name + " " + student.lastName + " призначено стипендію");
        //     break;
        // }
        switch (true) {
            case obj[key] < 4 :
                return alert("Кількість поганих (менше 4) оцінок: " + getLenght(student.table));
            default :
                if (getAverageMark(student.table) <= 7) {
                    return alert(student.name + " " + student.lastName + " переведено на наступний курс");
                } else {
                    alert(student.name + " " + student.lastName + " переведено на наступний курс");
                    return alert(student.name + " " + student.lastName + " призначено стипендію");
                }
        }
    }
}

getResult(student.table);*/
