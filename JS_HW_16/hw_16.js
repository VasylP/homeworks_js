"use strict";

/** Задание
 Реализовать функцию для подсчета n-го обобщенного числа Фибоначчи.
 Задача должна быть реализована на языке javascript,
 без использования фреймворков и сторонник библиотек (типа Jquery).

 Технические требования:
 Написать функцию для подсчета n-го обобщенного числа Фибоначчи.
 Аргументами на вход будут три числа - F0, F1, n, где F0, F1 - первые два числа последовательности
 (могут быть любыми целыми числами), n - порядковый номер числа Фибоначчи, которое надо найти.
 Последовательнось будет строиться по следующему правилу F2 = F0 + F1, F3 = F1 + F2 и так далее.

 Считать с помощью модального окна браузера число, которое введет пользователь (n).

 С помощью функции посчитать n-е число в обобщенной последовательности Фибоначчи и вывести его на экран.
 Пользователь может ввести отрицательное число - результат надо посчитать по такому же правилу (F-1 = F-3 + F-2).
 */

let f0;
let f1;
let n;

do {
    f0 = prompt("Введіть перше число для послідовності чисел");
    f1 = prompt("Введіть друге число для послідовності чисел");
    n = prompt("Введіть порядкове значення числа Фібоначчі");
} while (isNaN(f0) || isNaN(f1) || isNaN(n) || !f0 || !f1 || !n);

function fibonacci (firstNum, secondNum, thirdNum) {
    if (thirdNum === 0) {
        return thirdNum;
    } else if (firstNum < 0 && secondNum < 0 && thirdNum < 0){
        let first = firstNum;
        let second = secondNum;
        let result = 0;
        for (let i = 0; i > thirdNum; i--){
            result = first + second;
            first = second;
            second = result;
        }
        return result;
    } else {
        let first = firstNum;
        let second = secondNum;
        let result = 0;
        for (let i = 0; i < thirdNum; i++){
            result = first + second;
            first = second;
            second = result;
        }
        return result;
    }
}

alert(fibonacci(+f0, +f1, +n));

/*
let userNumber;


    do {
    userNumber = +prompt("Enter your number");
} while (isNaN(userNumber));

function getFibonacci (num) {
    if (num === 0 || num === 1) {
        return num;
    } else if (num < 0) {
        return getFibonacci (num + 1) + getFibonacci (num + 2);
    } else {
        return getFibonacci (num - 1) + getFibonacci (num - 2);
    }
}

if (userNumber < 0) {
    alert(`F${userNumber} = F${userNumber + 2} + F${userNumber + 1} = -${getFibonacci(userNumber)}`);
} else {
    alert(`F${userNumber} = F${userNumber - 2} + F${userNumber - 1} = ${getFibonacci(userNumber)}`);
}
*/

/*function getPositiveFibonacci (stop) {
        let newf0 = 1;
        let newf1 = 1;
        let result  = 0;
        for (let i = 2; i < stop; i++) {
            result = newf0 + newf1;
            newf0 = newf1;
            newf1 = result;
        }
        return result;
    }

function getNegativeFibonacci (stop) {
        let newf0 = -1;
        let newf1 = -1;
        let result  = 0;
        for (let i = -2; i > stop; i--) {
            result = newf0 + newf1;
            newf0 = newf1;
            newf1 = result;
        }
        return result;
}

let f0;
let f1;
let n;

do {
    f0 = prompt("Введіть перше число");
    f1 = prompt("Введіть друге число");
    n = prompt("Введіть порядкове значення Фібоначчі");
} while (isNaN(f0) || isNaN(f1) || isNaN(n));

switch (true) {
    case (n === 0 || n === 1 || n === -1):
        alert(n);
        break;
    case (f0 > 0 || f1 > 0 || n > 0):
         alert(`${getPositiveFibonacci(n)} = ${getPositiveFibonacci(n - 1)} + ${getPositiveFibonacci(n -2)}`);
         break;
    case (f0 < 0 || f1 < 0 || n < 0):
        alert(`${getNegativeFibonacci(n)} = ${getNegativeFibonacci(n - -1)} + ${getNegativeFibonacci(n - -2)}`);
        break;
}*/

