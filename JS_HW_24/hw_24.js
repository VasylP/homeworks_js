"use strict";

/**
 Завдання
 Написати реалізацію гри "Сапер".
 Завдання має бути виконане на чистому Javascript
 без використання бібліотек типу jQuery або React.

 Технічні вимоги:
 - Намалювати на екрані поле 8*8 (можна використовувати таблицю чи набір блоків).
 - Згенерувати на полі випадковим чином 10 мін. Користувач не бачить, де вони знаходяться.
 - Клік лівою кнопкою по осередку поля "відкриває" її вміст користувачеві.
 Якщо в цій клітинці знаходиться міна, гравець програв.
 У такому разі показати всі інші міни на полі.
 Інші дії стають недоступними, можна лише розпочати нову гру.
 - Якщо міни немає, показати цифру - скільки мін знаходиться поруч із цїєю клітинкою.
 Якщо клітинка порожня (поряд з нею немає жодної міни) - необхідно відкрити всі сусідні
 клітинки з цифрами.
 - Клік правої кнопки миші встановлює або знімає із "закритої" клітинки прапорець міни.
 - Після першого ходу над полем має з'являтися кнопка "Почати гру заново",
 яка обнулюватиме попередній результат проходження та заново ініціалізуватиме поле.
 - Над полем має показуватись кількість розставлених прапорців,
 та загальна кількість мін (наприклад 7 / 10).

 Необов'язкове завдання підвищеної складності
 - При подвійному кліку на клітинку з цифрою -
 якщо навколо неї встановлено таку ж кількість прапорців,
 що зазначено на цифрі цієї комірки, відкривати всі сусідні комірки.
 Додати користувачеві можливість самостійно вказувати розмір поля.
 - Кількість мін на полі можна вважати за формулою Кількість мін = кількість осередків / 6.
 */
// отримати контейнер для розміщення ігрового поля
const wrapper = document.querySelector('.wrapper');

// створити матрицю, яка приймає розміри поля
function createMatrix (size) {
    const matrix = [];
    let idCount = 1;
    for (let y = 0; y < size; y++) {
        const row = [];
        for (let x = 0; x < size; x++) {
            row.push({
                id: idCount++,
                left: false,
                right: false,
                dbLeft: false,
                show: false,
                flag: false,
                mine: false,
                poten: false,
                disabled: false,
                number: 0,
                x,
                y,
            });
        }
        matrix.push(row);
    }
    return matrix;
}

// створити функцію, яка буде рандомно обирати пусті клітинки
function getFreeCells (matrix) {
    const freeCells = [];
    for (let y = 0; y < matrix.length; y++) {
        for (let x = 0; x < matrix[y].length; x++) {
            const cell = matrix[y][x];
            if (!cell.mine){
            freeCells.push(cell);
            }
        }
    }
    const index = Math.floor(Math.random() * freeCells.length);
    return freeCells[index];
}
// створити функцію, яка буде розставляти міни
function createMine (matrix) {
    const cell = getFreeCells(matrix);
    const cells = aroundCells(matrix, cell.x, cell.y)

    cell.mine = true;

    for (const cell of cells) {
        cell.number += 1;
    }
}

// оримати клітинку
function getCell (matrix, x, y){
    if (!matrix[y] || !matrix[y][x]){
        return false;
    }
    return matrix[y][x];
}
// створити функцію, яка буде перевіряти клітинки
function aroundCells (matrix, x, y){
    const cells = [];
    for (let dx = -1; dx <= 1; dx++){
        for (let dy = -1; dy <= 1; dy++){
            if (dx === 0 && dy === 0){
                continue;
            }
            const cell = getCell(matrix, x + dx, y + dy);
            if (cell){
                cells.push(cell);
            }
        }
    }
    return cells;
}
// створити функцію, яка буде створювати на основі матриці розмітку
function createHtml (matrix) {
    const gameField = document.createElement('div');
    gameField.classList.add('game-field');
    for (let y = 0; y < matrix.length; y++) {
        const rowDiv = document.createElement('div');
        rowDiv.classList.add('row');
        for (let x = 0; x < matrix[y].length; x++) {
            const cell = matrix[y][x];
            const cellDiv = document.createElement('div');
            cellDiv.classList.add('cell');
            rowDiv.append(cellDiv);
            cellDiv.oncontextmenu = () => false;
            cellDiv.setAttribute('data-cell-id', cell.id);
            if (cell.flag) {
                cellDiv.innerHTML = '&#128681';
                cellDiv.style.backgroundColor = "#FFFFFF";
                continue;
            }
            if (cell.disabled){
                cellDiv.style.pointerEvents = "none";
            }
            if (cell.poten){
                cellDiv.innerHTML = '?';
                cellDiv.style.backgroundColor = "#FDBF01FF";
                continue;
            }
            if (!cell.show) {
                cellDiv.innerHTML = ' ';
                continue;
            }
            if (cell.mine) {
                cellDiv.innerHTML = '&#128163';
                cellDiv.style.backgroundColor = "#FFFFFF";
                continue;
            }
            if (cell.number) {
                cellDiv.innerHTML = cell.number;
                cellDiv.style.backgroundColor = "#FFFFFF";
                continue;
            }
            cellDiv.innerHTML = '';
            cellDiv.style.backgroundColor = "#FFFFFF";
        }
        gameField.append(rowDiv);
    }
    return gameField;
}

// створити функцію, яка буде додавати в ДОМ ігрове поле
let gameOn = true;

function addGame (node = document.body, matrix){
    if (!gameOn){
        return;
    }

    const gameField = createHtml(matrix);
    node.innerHTML = '';
    node.append(gameField);

    const newGameButton = document.createElement('button');
    newGameButton.classList.add('new-game');
    newGameButton.innerText = "Почати гру заново";
    node.prepend(newGameButton);

    const infoBlock = document.createElement('span');
    infoBlock.classList.add('info-block');
    infoBlock.innerHTML = `&#128681 ${flagsSum(matrix)} / ${Math.floor((inputSize.value * inputSize.value) / 6)} &#128163 `;
    node.prepend(infoBlock);

    newGameButton.addEventListener('click', () => {
        gameOn = true;
        let field = createMatrix(inputSize.value);
        setMines(Math.floor((inputSize.value * inputSize.value) / 6), field);
        addGame(wrapper, field);
    });

    node.querySelectorAll('.cell').forEach(el => {
        el.addEventListener('click', (event) =>{
            mouseLeft(event, matrix, node);
        });
        el.addEventListener('contextmenu', (event) =>{
            mouseRight(event, matrix, node);
        });
        el.addEventListener('dblclick', (event) =>{
            mouseDbl(event, matrix, node);
        });
        el.addEventListener('mouseleave', (event) =>{
            mouseLeave(event, matrix, node);
        });
        el.addEventListener('mousemove', () => {
            for (let y = 0; y < matrix.length; y++) {
                for (let x = 0; x < matrix[y].length; x++) {
                    const cell = matrix[y][x];

                    if (cell.poten === true) {
                        return cell.poten = false;
                    }
                }
            }
        });
    });
    if (losing(matrix)){
        const loseContainer = document.createElement('div');
        loseContainer.classList.add('lose-container');
        const closeContainer = document.createElement('button');
        closeContainer.classList.add('close-button');
        closeContainer.innerText = 'X';
        loseContainer.append(closeContainer);
        document.body.prepend(loseContainer);
        closeContainer.addEventListener('click', ()=>{
            loseContainer.remove();
        });
        gameOn = false;
    } else if (win(matrix)){
        const winContainer = document.createElement('div');
        winContainer.classList.add('win-container');
        const closeContainer = document.createElement('button');
        closeContainer.classList.add('close-button');
        closeContainer.innerText = 'X';
        winContainer.append(closeContainer);
        document.body.prepend(winContainer);
        closeContainer.addEventListener('click', ()=>{
            winContainer.remove();
        });
        gameOn = false;
    }
}

// розкидати міни
function setMines (mineSum, matrix) {
    for (let i = 0; i < mineSum; i++) {
        createMine(matrix);
    }
}
// створити функції для кліків мишкою по полю
function getCellByID (matrix, id) {
    for (let y = 0; y < matrix.length; y++) {
        for (let x = 0; x < matrix[y].length; x++) {
            const cell = matrix[y][x];

            if (cell.id === id) {
                return cell;
            }
        }
    }
    return false;
}

function getInfo (event, matrix) {
    const element = event.target;
    const cellId = parseInt(element.getAttribute('data-cell-id'));
    return {
        left: event.which === 1,
        right: event.which === 3,
        cell: getCellByID(matrix, cellId)
    }
}
function mouseLeave (event, matrix, node){
    const info = getInfo(event, matrix);
    info.cell.left = false;
    info.cell.right = false;
    addGame(node, matrix);
}
function mouseLeft (event, matrix, node) {
    const info = getInfo(event, matrix);
    if (info.left) {
        info.cell.left = true
    }
    leftClick(info.cell, matrix);
    addGame(node, matrix);
}
function mouseRight (event, matrix, node) {
    const info = getInfo(event, matrix);
    if (info.right) {
        info.cell.right = true
    }
    rightClick(info.cell);
    addGame(node, matrix);
}
function mouseDbl (event, matrix, node) {
    const info = getInfo(event, matrix);
    info.cell.dbLeft = true;
    dblClick(info.cell, matrix);
    addGame(node, matrix);
}

function leftClick (cell, matrix){
    if (cell.show || cell.flag){
        return;
    }
    cell.show = true;

    if (!cell.number) {
        openAroundCells(cell, matrix);
    }

    if (cell.mine){
        const mineCells = [];
        const cells = [];
        for (let y = 0; y < matrix.length; y++) {
            for (let x = 0; x < matrix[y].length; x++) {
                const cell = matrix[y][x];
                if (cell.mine === false) {
                    cells.push(cell);
                }
                if (cell.mine === true) {
                    mineCells.push(cell);
                }
            }
        }
        mineCells.forEach(el => {
            el.show = true;
        });
        cells.forEach(element => {
            element.disabled = true;
        });
    }
}

function openAroundCells (cell, matrix){
        const matr = matrix;
        const cells = aroundCells(matrix, cell.x, cell.y);

        cells.forEach(el => {
            if (!el.mine && !el.show && !el.flag){
                el.show = true;
                if (!el.number){
                openAroundCells(el, matr);
                }
            }
        });
}

function flagsSum (matrix){
    const flags = [];
    for (let y = 0; y < matrix.length; y++) {
        for (let x = 0; x < matrix[y].length; x++) {
            const cell = matrix[y][x];
            if (cell.flag === true) {
                flags.push(cell);
            }
        }
    }
    return flags.length;
}

function rightClick (cell){
    if (!cell.show){
        cell.flag = !cell.flag;
    }
}

function dblClick (cell, matrix) {
    if (!cell.number || cell.mine || cell.flag){
        return;
    }
    const cells = aroundCells(matrix, cell.x, cell.y);
    const flags = cells.filter(x => x.flag).length;

    if (flags === cell.number){
        cells.filter(x => !x.flag && !x.show).forEach(cell => cell.show = true);
    } else {
        cells.filter(x => !x.flag && !x.show).forEach(cell => cell.poten = true);
    }
}

function win (matrix) {
    const flags = [];
    const mines = [];
    for (let y = 0; y < matrix.length; y++) {
        for (let x = 0; x < matrix[y].length; x++) {
            const cell = matrix[y][x];
            if (cell.flag === true) {
                flags.push(cell);
            }
            if (cell.mine === true) {
                mines.push(cell);
            }
        }
    }
    if (flags.length !== mines.length){
        return false;
    }
    for (const cell of mines){
        if (cell.flag === false) {
            return false;
        }
    }
    for (let y = 0; y < matrix.length; y++) {
        for (let x = 0; x < matrix[y].length; x++) {
            const cell = matrix[y][x];
            if (!cell.mine && !cell.show) {
                return false;
            }
        }
    }
    return true;
}

function losing (matrix) {
    for (let y = 0; y < matrix.length; y++) {
        for (let x = 0; x < matrix[y].length; x++) {
            const cell = matrix[y][x];
            if (cell.mine && cell.show) {
                return true;
            }
        }
    }
    return false;
}

// створити поле для вводу розміру поля
const inputSize = document.createElement('input');
inputSize.setAttribute('type', 'number');
inputSize.classList.add('field-size');
inputSize.setAttribute('placeholder', 'Введіть розмір поля');
wrapper.append(inputSize);
// створити кнопку для початку гри
const startButton = document.createElement('button');
startButton.classList.add('start-button');
startButton.innerText = "Почати гру !";
wrapper.append(startButton);

// створити обробник події для поля

wrapper.addEventListener('click', (event) => {
    let field;
    if (event.target.classList.contains('start-button')){
        if (inputSize.value >= 4){
        field = createMatrix(inputSize.value);
        setMines(Math.floor((inputSize.value * inputSize.value) / 6), field);
        addGame(wrapper, field);
        } else {
            const alertContainer = document.createElement('div');
            alertContainer.classList.add('alert-container');
            alertContainer.innerHTML = '<p style="font-size: 24px; font-weight: 600; text-align: center; color: red; margin: 40px">Розмір поля не може бути менше ніж 4 колоноки</p>';
            const closeAlertContainer = document.createElement('button');
            closeAlertContainer.classList.add('close-button');
            closeAlertContainer.innerText = 'X';
            alertContainer.prepend(closeAlertContainer);
            document.body.prepend(alertContainer);
            closeAlertContainer.addEventListener('click', ()=>{
                alertContainer.remove();
            });
        }
    }
});

const restartButton = document.querySelector('#restart');
restartButton.addEventListener('click', () => {
    window.location.reload();
});
