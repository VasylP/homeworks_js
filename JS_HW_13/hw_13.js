"use strict";

/**
 -- Теоретичні питання --
 1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
 2. Що станеться, якщо в функцію setTimeout() передати нульову затримку?
 Чи спрацює вона миттєво і чому?
 3. Чому важливо не забувати викликати функцію clearInterval(),
 коли раніше створений цикл запуску вам вже не потрібен?

 -- Відповідь --
 1. setTimeout() - виконується (запускає функцію) один раз через певний (заданий час);
 setInterval() - виконується (запускає функцію) постійно через певний (заданий час),
 поки не настане умова при якій цей цикл запусків потрібно зупинити.

 2. При передачі нульового значення, буде визвана функція настільки швидко
 на скільки це можливо, але спочатку буде виконаний поточний код, тому це не гарантує
 миттєвий виклик.

 3. clearInterval() - потрібно використовувати для того, щоб не перевантажувати пам'ять.
 setInterval() - залишається в пам'яті до ти, доки не буде визвано setInterval().

 -- Завдання --
 Реалізувати програму, яка циклічно показує різні картинки.
 Завдання має бути виконане на чистому Javascript без використання
 бібліотек типу jQuery або React.

 -- Технічні вимоги:
 У папці banners лежить HTML код та папка з картинками.
 При запуску програми на екрані має відображатись перша картинка.
 Через 3 секунди замість неї має бути показано друга картинка.
 Ще через 3 секунди – третя.
 Ще через 3 секунди – четверта.
 Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
 Після запуску програми десь на екрані має з'явитись кнопка з написом Припинити.
 Після натискання на кнопку Припинити цикл завершується,
 на екрані залишається показаною та картинка,
 яка була там при натисканні кнопки.
 Поруч із кнопкою Припинити має бути кнопка Відновити показ,
 при натисканні якої цикл триває з тієї картинки,
 яка в даний момент показана на екрані.
 Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
 Необов'язкове завдання підвищеної складності
 При запуску програми на екрані повинен бути таймер з секундами
 та мілісекундами, що показує, скільки залишилося до показу наступної картинки.
 Робити приховування картинки та показ нової картинки поступовим
 (анімація fadeOut/fadeIn) протягом 0.5 секунди.
 */

// Варіант вирішення без необов'язкового завдання
/*const wrapper = document.querySelector('.images-wrapper');
wrapper.style.cssText = "width: 400px; height: 400px; margin: auto; overflow: hidden; border: 5px solid black;";

const images = document.querySelectorAll(".image-to-show");

const buttonContainer = document.createElement('div');
buttonContainer.classList.add('button-container');


const stopButton = document.createElement('button');
stopButton.classList.add('stop-button');
stopButton.innerText = 'Stop demo';

const resumeButton = document.createElement('button');
resumeButton.classList.add('resume-button');
resumeButton.innerText = 'Resume demo';

buttonContainer.append(stopButton);
buttonContainer.append(resumeButton);
document.body.append(buttonContainer);

let counter = 0;
let step = images[0].clientHeight;

let timer;
let ms = 3000;

function slider () {
    counter++;
    if (counter >= images.length){
        counter = 0;
    }
    images.forEach(el => {
        el.style.transform = 'translateY(' + `${-step * counter}px)`;
    })
    timer = setTimeout(slider, ms);
}

setTimeout(slider, ms);

buttonContainer.addEventListener('click', (e) => {
    if (e.target.classList.contains('stop-button')){
        clearTimeout(timer);
    }
    if (e.target.classList.contains('resume-button')){
        setTimeout(slider, ms);
    }
});*/

//Варіант з необов'язковим завданням
const wrapper = document.querySelector('.images-wrapper');
wrapper.style.cssText = "width: 400px; height: 400px; margin: auto; overflow: hidden; border: 5px solid black;";

const images = document.querySelectorAll(".image-to-show");

const buttonContainer = document.createElement('div');
buttonContainer.classList.add('button-container');
buttonContainer.innerHTML =
    '<button class="stop-button">Stop demo</button><button class="resume-button">Resume demo</button>';
document.body.append(buttonContainer);

const timerSection = document.createElement('p');
timerSection.classList.add('timer-section');

let ms = 0;
let sec = 3;

let timer;

let counter = 0;

function slider () {
    fadeOut(images[counter]);
    counter++;
    if (counter >= images.length) {
        counter = 0;
    }
        fadeIn(images[counter]);
}


function fadeOut (el) {
    let opacity = 1;
    let timerSlider = setInterval(function (){
        if (opacity <=0.1){
            clearInterval(timerSlider);
            el.style.display = "none";
        }
        el.style.opacity = `${opacity}`;
        opacity -= opacity * 0.1;
    }, 25);
}


    function fadeIn (el) {
    let opacity = 0.01;
    el.style.display = "block";
    let timerIn = setInterval(function() {
        if(opacity >= 1) {
            clearInterval(timerIn);
        }
        el.style.opacity = opacity;
        opacity += opacity * 0.1;
    }, 25);
    }


function animation () {
    wrapper.after(timerSection);
    document.body.append(buttonContainer);

    timerSection.innerHTML = `sec: ${sec}  ms: ${ms -= 10}`;
    if ( ms <= 0) {
        if (sec === 0){
            sec = 3;
            ms = 0;
        }
        if (sec === 1){
            slider();
        }
        sec--;
        ms = 1000;
    }
    timer = setTimeout(animation, 10);
}


setTimeout(animation, 1000);

/*buttonContainer.addEventListener('click', (e) => {
    if (e.target.classList.contains('stop-button')) {
        clearTimeout(timer);
        console.log('Hi');
    }
    if (e.target.classList.contains('resume-button')) {
        setTimeout(animation, ms);
    }
});*/

document.querySelector('.stop-button').addEventListener('click', () => {
    clearTimeout(timer);
    console.log('Hi');
});

document.querySelector('.resume-button').addEventListener('click', () => {
    setTimeout(animation, ms);
});