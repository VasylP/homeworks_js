"use strict";
/*
Теоретичні питання
   * Описати своїми словами навіщо потрібні функції у програмуванні.
   * Описати своїми словами, навіщо у функцію передавати аргумент.
   * Що таке оператор return та як він працює всередині функції?

    Завдання
    Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.
    Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

    Технічні вимоги:
    Отримати за допомогою модального вікна браузера два числа.
    Отримати за допомогою модального вікна браузера математичну операцію,
    яку потрібно виконати. Сюди може бути введено +, -, *, /.
Створити функцію, в яку передати два значення та операцію.
    Вивести у консоль результат виконання функції.
   * Необов'язкове завдання підвищеної складності
Після введення даних додати перевірку їхньої коректності.
    Якщо користувач не ввів числа, або при вводі вказав не числа, -
    запитати обидва числа знову
(при цьому значенням за замовчуванням для кожної зі змінних має бути введена інформація раніше).
*/

/** Описати своїми словами навіщо потрібні функції у програмуванні.
 Функції потрібні для того, щоб оптимізувати написання коду та не повторювати один і той самий код по кілька разів.
 Функції дозволяють робити певні дії кілька разів без повторення коду.

 * Описати своїми словами, навіщо у функцію передавати аргумент.
 * У функцію необхідно передавати аргумент для того щоб функція була виконана, тобто щоб визвати функцію.

 Для того, щоб функція була виконана вірно та був виведений результат такої функції
 їй необхідно передати аргумент.

 * Що таке оператор return та як він працює всередині функції?
 Цей оператор завершує виконання функції і повертає задане значення.
 */



let firstNumber = prompt('Please enter first number');
let sign = prompt('Please enter, what you want doing with first number (+, -, *, /)');
let secondNumber = prompt('Please enter second number');

if (firstNumber === null){
    firstNumber = "";
}
if (sign === null){
    sign = "";
}
if (secondNumber === null){
    secondNumber = "";
}

while (!firstNumber || !secondNumber || isNaN(firstNumber) || isNaN(secondNumber) || !sign || +sign ||
sign !== '+' && sign !== '-' && sign !== '*' && sign !== '/') {

    firstNumber = prompt('Please enter first number', firstNumber);
    sign = prompt('Please enter, what you want doing with first number (+, -, *, /)', sign);
   secondNumber = prompt('Please enter second number', secondNumber);
    if (firstNumber === null){
        firstNumber = "";
    }
    if (sign === null){
        sign = "";
    }
    if (secondNumber === null){
        secondNumber = "";
    }
}

function getCalc(firstNum, x, secondNum){
    switch (true) {
        case (x === '+'):
            return +firstNum + +secondNum;
        case (x === '-'):
            return +firstNum - +secondNum;
        case (x === '*'):
            return +firstNum * +secondNum;
        case (x === '/'):
            return +firstNum / +secondNum;
    }
}
console.log(getCalc(firstNumber, sign, secondNumber));

// function getCalc(firstNum, x, secondNum){
//     switch (true) {
//         case (x === '+'):
//             return +firstNum + +secondNum;
//         case (x === '-'):
//             return +firstNum - +secondNum;
//         case (x === '*'):
//             return +firstNum * +secondNum;
//         case (x === '/'):
//             return +firstNum / +secondNum;
//     }
// }
//
// let firstNumber;
// do {
//     firstNumber = prompt('Please enter first number');
// } while (!firstNumber || isNaN(firstNumber));
//
// let secondNumber;
// do {
//     secondNumber = prompt('Please enter second number');
// } while (!secondNumber || isNaN(secondNumber));
//
// let signs;
// do {
//     signs = prompt('Please enter, what you want doing with first number (+, -, *, /)');
// } while (signs !== '+' && signs !== '-' && signs !== '*' && signs !== '/' || !isNaN(signs));
//
// console.log(getCalc(firstNumber, signs, secondNumber));