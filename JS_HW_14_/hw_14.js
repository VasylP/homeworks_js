"use strict";

/**
 Завдання
 Реалізувати можливість зміни колірної теми користувача.
 Завдання має бути виконане на чистому Javascript без використання
 бібліотек типу jQuery або React.

 Технічні вимоги:
 Взяти будь-яке готове домашнє завдання з HTML/CSS.
 Додати на макеті кнопку "Змінити тему".
 При натисканні на кнопку - змінювати колірну гаму сайту
 (кольори кнопок, фону тощо) на ваш розсуд.
 При повторному натискання - повертати все як було спочатку -
 начебто для сторінки доступні дві колірні теми.
 Вибрана тема повинна зберігатися після перезавантаження сторінки
 */

const logoSpan = document.querySelector('.logo-link__span');
const btn = document.querySelector('#btn');
const asideMenu = document.querySelector('.aside-menu');
const pictureDiv = document.querySelector('.main-content__picture');
const firstText = document.querySelector('.main-text_first');
const secondText = document.querySelector('.main-text_second');
const footerMenu = document.querySelector('.footer-menu');

btn.addEventListener('click', () => {
    if (!btn.classList.contains('active')){
        btn.classList.add('active');
        localStorage.setItem('background', document.body.style.backgroundColor = 'black');
        localStorage.setItem('logo span', logoSpan.style.color = '#FFFFFF');
        localStorage.setItem('button color', btn.style.cssText = `background-color: #000000; color: #FFFFFF`);
        localStorage.setItem('aside border', asideMenu.style.borderColor = '#4BCAFF');
        localStorage.setItem('picture border', pictureDiv.style.borderColor = '#4BCAFF');
        localStorage.setItem('aside text first', firstText.style.color = '#FFFFFF');
        localStorage.setItem('aside text second', secondText.style.color = '#FFFFFF');
        localStorage.setItem('footer background', footerMenu.style.backgroundColor = '#35444F');
    } else {
        btn.classList.remove('active');
        document.body.style.backgroundColor = '';
        logoSpan.style.color = '';
        btn.style.cssText = '';
        asideMenu.style.borderColor = '';
        pictureDiv.style.borderColor = '';
        firstText.style.color = '';
        secondText.style.color = '';
        footerMenu.style.backgroundColor = '';
        localStorage.clear();
    }
});

function checkStorage() {
    document.body.style.backgroundColor = localStorage.getItem('background');
    logoSpan.style.color = localStorage.getItem('logo span');
    btn.style.cssText = localStorage.getItem('button color');
    asideMenu.style.borderColor = localStorage.getItem('aside border');
    pictureDiv.style.borderColor = localStorage.getItem('picture border');
    firstText.style.color = localStorage.getItem('aside text first');
    secondText.style.color = localStorage.getItem('aside text second');
    footerMenu.style.backgroundColor = localStorage.getItem('footer background');
}

checkStorage();