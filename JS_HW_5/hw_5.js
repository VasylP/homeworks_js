"use strict";

/** Теоретичні питання
- Опишіть своїми словами, що таке метод об'єкту
- Який тип даних може мати значення властивості об'єкта?
- Об'єкт це посилальний тип даних. Що означає це поняття?

 * Метод - це функція в об'єкті;
 *Значення можуть бути будь-якого типу;
 *Якщо необхідно скопіювати об'єкт (створити новий з певними властивостями іншого об'єкту),
 то об'єкт який копіюється, зберігається як посилання.

 * Завдання
 Реалізувати функцію створення об'єкта "юзер".
 Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

 Технічні вимоги:
 Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
 При виклику функція повинна запитати ім'я та прізвище.
 Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
 Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера,
 з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
 Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin().
 Вивести у консоль результат виконання функції.
 Необов'язкове завдання підвищеної складності
 Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму.
 Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.
 */

function createNewUser (){
    const newUser = {
        userName: {writable: false, configurable: true, value: ""},
        userLastName: {writable: false, configurable: true, value: ""},
        setName (newName) {
            Object.defineProperty(this, 'userName', {value: newName});
        },
        setSurname (newLastName) {
            Object.defineProperty(this, 'userLastName', {value: newLastName});
        },
        getLogin () {
            return this.userName.toLowerCase().charAt(0) + this.userLastName.toLowerCase();
        },
    }
    newUser.setName(prompt("Будь ласка введіть своє ім'я"));
    newUser.setSurname(prompt("Будь ласка введіть своє прізвище"));
    console.log(newUser.getLogin());
    return newUser;
}
createNewUser();

/*
function createNewUser(){
    let userName;
    let userLastName;
    do {
        userName = prompt("Будь ласка введіть своє ім'я");
        userLastName = prompt("Будь ласка введіть своє прізвище");
    } while (!userName || !userLastName);
    const newUser = {
        firstName: userName,
        lastName: userLastName,
        getLogin() {
            return this.firstName.toLowerCase().charAt(0) + this.lastName.toLowerCase();
        },
        setName (newFName) {
            this.firstName = newFName;
        },
        setSurname (newLName) {
            this.lastName = newLName;
        }
    };
    Object.defineProperties(newUser, {firstName: {writable:false}, lastName: {writable:false},});
    console.log(newUser.getLogin());
    return newUser;
}

createNewUser();*/
/*
const user = {
    firstName: {writable: false, configurable: true, value: ""},
    lastName: {writable: false, configurable: true, value: "prompt()"},
    setName (newName) {
        Object.defineProperty(this, 'firstName', {value: newName});
    },
    setSurname (newLName) {
        Object.defineProperty(this, 'lastName', {value: newLName});
    },
    getLogin() {
        return this.firstName.toLowerCase().charAt(0) + this.lastName.toLowerCase();
    },
}


console.log(user.getLogin());
console.log(user);


function createNewUser(){
    let userName;
    let userLastName;
    do {
        userName = prompt("Будь ласка введіть своє ім'я");
        userLastName = prompt("Будь ласка введіть своє прізвище");
    } while (!userName || !userLastName);
    const newUser = Object.assign({}, user);
    newUser.firstName = userName;
    newUser.lastName = userLastName;
    console.log(newUser.getLogin());
    return newUser;
}

createNewUser();*/