"use strict";
/**
 Завдання
 Створити таблицю, при натисканні на клітинки якої вони змінюватимуть колір.
 Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

 Технічні вимоги:
 Створити поле 30*30 з білих клітинок за допомогою елемента <table>.
 При натисканні на білу клітинку вона повинна змінювати колір на чорний.
 При натисканні на чорну клітинку вона повинна змінювати колір назад на білий.
 Сама таблиця повинна бути не вставлена у вихідний HTML-код, а згенерована і додана
 в DOM сторінки за допомогою Javascript.
 Обработчик события click нужно повесить на всю таблицу.
 События всплывают от элемента вверх по DOM дереву, их все можно ловить с
 помощью одного обработчика событий на таблице, и в нем определять, на какую из ячеек нажал пользователь.
 При клике на любое место документа вне таблицы, все цвета клеточек должны
 поменяться на противоположные (подсказка: нужно поставить Event Listener на <body>).
 Чтобы поменять цвета всех клеточек сразу, не нужно обходить их в цикле.
 Если помечать нажатые клетки определенным классом, то перекрасить их все
 одновременно можно одним действием - поменяв класс на самой таблице.
 */

const table = document.createElement('table');
document.body.append(table);

function createTable (rows, columns){
for (let i = 0; i < rows; i++) {
    document.querySelector('table').append(document.createElement('tr'));
}
document.querySelectorAll('tr').forEach(element => {
    for (let i = 0; i < columns; i++) {
        element.append(document.createElement('td'));
    }
})
}

createTable(30, 30);

const column = table.querySelectorAll('td');
column.forEach(element => {
    element.style.cssText = 'background-color: #FFFFFF; border: 1px solid green; width: 26px; height: 26px;';
    element.classList.add('cell');
});

table.addEventListener('click', (event) => {
    if (event.target.classList.contains('active')){
        event.target.classList.remove('active');
        event.target.style.backgroundColor = '#FFFFFF';
    } else  {
        table.querySelectorAll('.active').forEach(element =>{
            element.classList.remove('active');
            element.style.backgroundColor = '#FFFFFF';
        });
        event.target.classList.add('active');
        event.target.style.backgroundColor = '#000000';
    }
})

document.body.addEventListener('click', (e) => {
    if (e.target.tagName === 'BODY'){
        table.querySelectorAll('.cell').forEach(element =>{
            element.style.backgroundColor = '#000000';
        });
        table.querySelectorAll('.active').forEach(element =>{
            element.style.backgroundColor = '#FFFFFF';
        });
    }
});
