"use strict";

/** Теоретичні питання
 Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці?

       Рекурсія - це функція яка визиває сама себе.
       На практиці рекурсія використовується для розбивки великої задачі на кілька більш простих задач.
 */

/** Завдання
 Реалізувати функцію підрахунку факторіалу числа. Завдання має бути виконане на чистому Javascript
 без використання бібліотек типу jQuery або React.

 Технічні вимоги:
 Отримати за допомогою модального вікна браузера число, яке введе користувач.
 За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
 Використовувати синтаксис ES6 для роботи зі змінними та функціями.

 Необов'язкове завдання підвищеної складності
 Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів число,
 або при введенні вказав не число, - запитати число заново
 (при цьому значенням для нього повинна бути введена раніше інформація).
 */

/*let userNumber = prompt("Please enter your number");

while (isNaN(userNumber) || !userNumber) {
    userNumber = +prompt("Please enter your number", userNumber);
}

function getFactorial (num) {
    if (num < 0) {
        alert("Factorial must have positive number");
    } else if (num === 0) {
        return 1;
    } else {
        return num * getFactorial(num - 1);
    }
}*/

// function getFactorial (num) {
//     return num ? num * getFactorial(num - 1) : 1;
// }

// function getFactorial (num) {
//     return num <= 0 ? 1 : getFactorial(num - 1);
// alert (getFactorial(userNumber));

let userNumber = prompt("Please enter your number");

while (isNaN(userNumber)) {
    userNumber = +prompt("Please enter your number", userNumber);
}

if (userNumber < 0) {
    alert("Factorial must have positive number");
} else {
    alert (getFactorial(userNumber));
}

function getFactorial (num) {
     if (num === 0) {
        return 1;
    } else {
        return num * getFactorial(num - 1);
    }
}