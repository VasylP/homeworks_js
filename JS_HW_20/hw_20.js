"use strict";
/**
 Завдання
 Реалізувати універсальний фільтр масиву об'єктів.
 Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

 Технічні вимоги:
 Написати функцію filterCollection(), яка дозволить відфільтрувати будь-який масив
 за заданими ключовими словами.
 Функція має приймати на вхід три основні аргументи:

 - масив, який треба відфільтрувати

 - Рядок з ключовими словами, які треба знайти всередині масиву
 (одне слово або кілька слів, розділених пробілом)

 - boolean прапор, який буде говорити, чи треба знайти всі ключові слова (true),
 або достатньо збігу одного з них (false)

 - четвертий і наступні аргументи будуть іменами полів,
 у яких треба шукати збіг.

 Якщо поле не на першому рівні об'єкта,
 до нього треба вказати повний шлях через .. Рівень вкладеності полів може бути будь-яким.

 Приклад виклику функції:
 filterCollection(vehicles, 'en_US Toyota', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description')

 В даному прикладі буде відфільтрований масив vehicles,
 за допомогою ключових слів en_US та Toyota. true у третьому параметрі означає,
 що для успішного включення запису у фінальний результат має бути збіг за обома ключовими словами.
 Останні кілька параметрів містять імена полів, у яких треба шукати ключові слова.
 Наприклад contentType.name означає, що всередині кожного об'єкта vehicle може бути поле contentType,
 яке є об'єктом або масивом об'єктів, усередині яких може бути поле name.
 Именно в этом поле (а также в других указанных) необходимо искать сопадения с ключевыми словами.
 У прикладі вище - запис locales.name означає, що поле locales всередині об'єкта vehicle
 може бути як об'єктом, так і масивом.
 Якщо воно є масивом, це означає, що всередині масиву знаходяться об'єкти,
 у кожного з яких може бути властивість name. Д
 ля успішної фільтрації достатньо знаходження ключового слова хоча б у одному з елементів масиву.
 Різні ключові слова можуть бути в різних властивостях об'єкта.Наприклад,
 у прикладі вище, ключове слово en_US може бути знайдено в полі locales.name,
 тоді як ключове слово Toyota може, наприклад, знаходитися всередині властивості description.
 При цьому такий об'єкт має бути знайдено.
 Пошук має бути нечутливим до регістру.
 Примітка:
 Реалізацію цього завдання можна використовувати у реальному житті.
 Наприклад, якщо на сторінці є таблиця з даними, а вгорі є рядок пошуку,
 цю функцію можна використовувати для фільтру значень у таблиці при введенні ключових слів у рядок пошуку
 */
let contentType = [{name: 'Nissan', description: "en_US",}];
let locales = {name: 'Toyota', description: "en_US",};
let test = {name: "US_en", description: 'Toyota',};

const vehicles = [contentType, locales, test];

/*let resultGetElement = [];
function getElement (arre) {

    arre.forEach(element => {
        if (!Array.isArray(element)) {
            resultGetElement.push(element);
                // for (let key in element){
                //     resultGetElement.push(element);
                // }
            // for (let key in element){
            //     for (let el in values) {
            //         if (element[key] === values[el]) {
            //             result.push(element[key]);
            //         }
            //     }
            // }
        } else  {
        getElement(element);
        }
    });
    return resultGetElement;
}

function objEct (obj) {
    for (let key in obj){
        if ((typeof obj[key]) == "object"){
            objEct(obj[key]);
        } else {

        }
    }
}


function checkAvailability(arr, val) {
    return arr.some(function(arrVal) {
        return  val === arrVal;
    });
}*/

function filterCollection(array, keyWord, bool, ...argument) {
    let wordsToSearch = keyWord.split(" ");
    let ar = [...argument];
    let newArr = [];
    function getNewArray(arr){
        arr.filter(element =>{
            if (!Array.isArray(element)){
                ar.forEach(el => {
                    if (el.split('.').join(' ') === Object.keys(element).join(' ')){
                        newArr.push(element);
                    }
                });
            } else {
                getNewArray(element);
            }
        });
    }
    getNewArray(array);
    let objKeys = [];
    for (let i =0; i < newArr.length; i++){
    objKeys.push(Object.keys(newArr[i]));
    }
    console.log(newArr);

}

filterCollection(vehicles, 'en_US Toyota', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description');

