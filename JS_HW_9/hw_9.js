"use strict";
/**
 -- Теоретичні питання --
 1. Опишіть, як можна створити новий HTML тег на сторінці.
 2. Опишіть, що означає перший параметр функції insertAdjacentHTML
 і опишіть можливі варіанти цього параметра.
 3. Як можна видалити елемент зі сторінки?

 -- Відповіді --
 1. Новий HTML тег на сторінці можна створити за допомогою .createElement(), але для
 того, щоб новий тег відобразився у HTML документі його обов'язково після створення необхідно
 добавити до документу.
 2. Перший параметр функції insertAdjacentHTML означає місце куди необхідно в документі вставити
 новостворений елемент чи текст.  Можуть бути наступні значення:
 - beforebegin : означає вставку перед елементом;
 - afterbegin : означає вставку в середині елементу одразу після відкриваючого тегу;
 - beforeend : означає вставку в середині елементу перед закриваючим тегом;
 - afterend : ознвчає вставку одразу після закриваючого тегу елементу.
 3. Видалити елемент зі сторінки можна за допомогою .remove().

 -- Завдання --
 Реалізувати функцію, яка отримуватиме масив елементів
 і виводити їх на сторінку у вигляді списку.
 Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

 Технічні вимоги:
 Створити функцію, яка прийматиме на вхід масив
 і опціональний другий аргумент parent - DOM-елемент,
 до якого буде прикріплений список (по дефолту має бути document.body.
 кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
 Приклади масивів, які можна виводити на екран:

 ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
 ["1", "2", "3", "sea", "user", 23];
 Можна взяти будь-який інший масив.

 Необов'язкове завдання підвищеної складності:
 Додайте обробку вкладених масивів.
 Якщо всередині масиву одним із елементів буде ще один масив,
 виводити його як вкладений список. Приклад такого масиву:

 ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
 Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
 Очистити сторінку через 3 секунди.
 Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.
 */

const arr1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const arr2 = ["1", "2", "3", "sea", "user", 23];
const arr3 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

function createNewArr(arr) {
    let newArr;
    newArr = `<ul>${arr.map(value => Array.isArray(value) ? createNewArr(value) : 
        `<li>${value}</li>`).join('')}</ul>`;
    return newArr;
}

function listItem (array, parent = document.body){
    parent.innerHTML = createNewArr(array);
    let timerDiv = document.createElement('div');
    document.body.append(timerDiv);
    function clearPage (i) {
        let timer = setTimeout(function (){
            clearPage(i-1);
            timerDiv.innerHTML = i + " sec";
            if (!i) parent.remove();
        }, 1000);
        if (i < 0) clearTimeout(timer);
    }
    clearPage(3);
}

listItem(arr3);

/*function listItem (array, parent = document.body) {
    const parentChild = document.createElement("ul");
    parent.append(parentChild);
    let list;
    array.forEach(element => {
        list = document.createElement('li');
        list.innerText = element;
        parentChild.append(list);
        if (Array.isArray(element)){
            const childUl = document.createElement("ul");
            list.append(childUl);
            element.forEach(elementLiArr => {
            list = document.createElement('li');
            list.innerText = elementLiArr;
            childUl.append(list);
            })
        }
    });
}
listItem(arr3);*/
