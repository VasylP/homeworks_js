"use strict";
/**
 -- Теоретичні питання --

 1. Опишіть своїми словами що таке Document Object Model (DOM)
 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

 1. DOM - це об'єктна модель документу.
 2.  innerHTML - показує вкладені, в певний тег, як текст так і інші теги.
 innerText - відображає лише текст і не відображає теги.
 3. Щоб звернутись до елементу сторінки за допомогою JS, краще використовувати
 document.querySelectorAll(".class").
 getElementById, getElementsByClassName, getElementsByTagName та інші.
 Тобто краще звертатись по класу або айді.
 І самий універсальний спосіб - це document.querySelectorAll("").


 -- Завдання --
 Код для завдань лежить в папці project.


 Знайти всі параграфи на сторінці та встановити колір фону #ff0000


 Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
 Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.


 Встановіть в якості контента елемента з класом testParagraph наступний параграф -
 This is a paragraph



 Отримати елементи

 , вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.


 Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
 */

const paragraph = document.querySelectorAll('p');
paragraph.forEach(element => {
    element.style.background = '#ff0000';
});

const idElement = document.querySelector('#optionsList');
console.log(idElement);

const parent = idElement.parentElement;
console.log(parent);

const child = idElement.children;
console.log(child);
// const child = idElement.childNodes;
// console.log(child);

const testP = document.querySelector('#testParagraph');
testP.innerText = 'This is a paragraph';
// testP.textContent = 'This is a paragraph';

const headerClass = document.querySelector('.main-header');
console.log(headerClass);
for (const element of headerClass.children) {
    element.classList.add("nav-item");
}

const sectionClassDel = document.querySelectorAll('.section-title');
for (const element of sectionClassDel) {
    element.classList.remove("section-title");
}
console.log(sectionClassDel);


