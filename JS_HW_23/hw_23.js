"use strict";

/**
 -- Теоретичні питання --

 1. Опишіть своїми словами, що таке обробник подій.
 2. Опишіть, як додати обробник подій до елемента. Який спосіб найкращий і чому?


 -- Завдання --
 Створити поле для введення ціни з валідацією.
 Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

 Технічні вимоги:

 При завантаженні сторінки показати користувачеві поле вводу (input) з написом Price.
 Це поле буде служити для введення числових значень
 Поведінка поля має бути наступною:

 При фокусі на полі вводу - має з'явитися рамка зеленого кольору.
 У разі втрати фокусу вона пропадає.
 Коли прибрано фокус з поля - його значення зчитується, над полем створюється span,
 у якому має бути текст: Поточна ціна: ${значення з поля вводу}.
 Поруч із ним має бути кнопка з хрестиком (X).
 Значення всередині поля вводу забарвлюється у зелений колір.
 При натисканні на Х - span з текстом та кнопка X повинні бути видалені.
 Значення, введене у поле вводу, обнулюється.
 Якщо користувач ввів число менше 0 – при втраті фокусу підсвічувати
 поле введення червоною рамкою, під полем виводити
 фразу – Please enter correct price. span з некорректним значенням при цьому не створюється.


 У папці img лежать приклади реалізації поля вводу та "span", які можна брати як приклад
 */

const input = document.querySelector('#price-input');
const text = document.createElement('span');
text.style.padding = "10px";
text.style.fontSize = '20px';
const icon = document.createElement('span');
icon.innerHTML = "&#10008";
icon.style.color = 'red';
icon.style.cursor = 'pointer';
icon.style.border = '1px solid grey';
icon.style.borderRadius = '10px';
icon.style.margin = '10px';
icon.style.padding = '3px';

input.addEventListener('blur', () => {
    input.style.cssText = '';
    if (input.value === '0'){
        input.style.border = '4px solid red';
        text.innerText = 'Please enter correct price.';
        text.style.border = '';
        text.style.color = 'red';
        document.querySelector('.wrapper').append(text);
    } else {
        input.style.color = 'green';
        text.innerText = `Поточна ціна: ${input.value} `;
        text.style.color = 'green';
        text.style.border = "2px solid grey";
        text.style.borderRadius = "25px";
        document.querySelector('.wrapper').prepend(text);
        text.append(icon);
    }
});

input.addEventListener('focus', () => {
    input.style.cssText = 'outline: none; border: 4px solid green; border-radius: 4px;';
    text.remove();
});

icon.addEventListener('click', () => {
    text.remove();
    input.value = '';
});